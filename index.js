'use strict';

var config    = require('./config');
var express   = require('express');
var log4js    = require('log4js');
var pg        = require('pg');
var validator = require('validator');

// Init log4js
log4js.configure('', config.log4js);

var ashLog = log4js.getLogger('akasha');
ashLog.info("log4js started");

// Init PG
var pgLog = log4js.getLogger('posgresql');

var conString = "pg://" + config.pg.user + ":" + config.pg.pass + "@" + config.pg.host + ":" + config.pg.port + "/" + config.pg.db;
pgLog.info("connecting to: %s", conString);
var ashDB   = new pg.Client(conString);

// Start Express
var app = express();
var port = process.env.PORT || config.port;

app.get('/', function(req, res){
  res.send('welcome');
});

app.listen(port, function(){
  ashLog.info('Running on PORT: %s', port);
});