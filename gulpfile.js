'use strict';

var gulp = require('gulp'),
    mocha = require('gulp-mocha'),
    nodemon = require('gulp-nodemon'),
    gutil = require('gulp-util');

gulp.task('devrun', function(){
  nodemon({
    script: 'index.js',
    ext: 'js',
    env: {
      PORT: 3000
         },
    ignore: ['./node_modules/**']
  });
});

gulp.task('mocha', function(){
  return gulp.src(['test/*.js'], {read:false})
    .pipe(mocha({reporter: 'list'}))
    .on('error', gutil.log);
});

gulp.task('watch-mocha', function(){
  gulp.run('mocha');
  gulp.watch(['./**/*.js', 'test/**/*.js'], ['mocha']);
});
